<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = ['quantity', 'name', 'price'];

  public function orders(){
  	return $this->belongsToMany('App\Order');
  }
}
