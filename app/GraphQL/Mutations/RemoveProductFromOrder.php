<?php

namespace App\GraphQL\Mutations;

use App\Product;
use App\Order;

class RemoveProductFromOrder
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
      $order = Order::find($args['order_id']);
      if ($order) {
      	$product = $order->products()->find($args['product_id']);;
      	if ($product) {
      		$order->products()->detach($product);
      		return $order;
      	}
      }
      return $order;
    }
}
