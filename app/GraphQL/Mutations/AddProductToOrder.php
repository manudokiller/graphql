<?php

namespace App\GraphQL\Mutations;

use App\Product;
use App\Order;

class AddProductToOrder
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
      $order = Order::find($args['order_id']);
      if ($order) {
      	$product = Product::find($args['product_id']);
      	if ($product) {
      		$order->products()->attach($product);
      		return $order;
      	}
      	return response()->json([
      		'error' => 'Product was not found'
      	])->getStatusCode(404);
      }
      return response()->json([
    		'error' => 'Order was not found'
    	])->getStatusCode(404);
    }
}
